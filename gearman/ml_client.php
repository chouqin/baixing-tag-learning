<?php

//liqiping@baixing.net

/**
 * This class is used for call category learning function using gearman
 */

class CategoryLearning {
  const CONN_TIMEOUT = 10000;
  const GEARMAN_IP = "192.168.1.40";
  private static $client;

  /**
   * @param string $title: 标题
   * @return 长度为3的数组，每一个数组元素的格式为["一级类目", "二级类目", "概率"]
   * 如果出现异常，返回一个空数组。
   */
  public static function getBytitle($title) {
    if (!is_string($title)) {
      return array();
    }

    $function_name = 'category_learning';

    try {
      $result = self::client()->doNormal("category_learning", $title);
    } catch (Exception $e){
      return array();
    }

    if (false === $result) {
      return array();
    } else {
      return json_decode($result);
    }
  }

  /**
   * 确保每次调用时使用的都是同一个client
   */
  private static function client() {
    if (!isset($client)) {
      self::$client = new GearmanClient();
      self::$client->setTimeout(self::CONN_TIMEOUT);
      self::$client->addServers(self::GEARMAN_IP);
    }

    return self::$client;
  }
}

var_dump(CategoryLearning::getBytitle("现有大批量高精仿手表对外转让 支持货到付款 验货满"));
var_dump(CategoryLearning::getBytitle("对折转让！！全新新款男式浪琴手表·海军上将GMT"));
