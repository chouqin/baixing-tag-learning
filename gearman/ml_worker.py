# -*- coding:utf-8 -*-

"""
author: liqiping@baixing.net
自动推荐类目的worker
使用python ml_worker.py启动一个worker
"""

from time import gmtime, strftime
import gearman
from sklearn.externals import joblib
import numpy as np
import json

# gearman的IP地址列表
GEARMAN_IPS = ["192.168.1.40"]


# 加载分类器
title_clf = joblib.load('../data/title_clf.pkl')
train_names = joblib.load('../data/train_names.pkl')
print "finish load model"


# 一级类目中英文名对应
first_category_name = {
    "qiuzhi": u"求职简历",
    "jiaoyupeixun": u"教育培训",
    "jianzhi": u"兼职招聘",
    "huodong": u"交友活动",
    "gongzuo": u"全职招聘",
    "fuwu": u"生活服务",
    "fang": u"房屋租赁",
    "ershou": u"物品交易",
    "chongwuleimu": u"宠物",
    "cheliang": u"车辆买卖",
}


with open("../namemap.json") as f:
    namemap = json.load(f)


with open("../parent_map.json") as f:
    parentmap = json.load(f)


# 输入： 信息的标题
# 输出： 一个长度为3的数组，每一个数组元素的格式为["一级类目", "二级类目", "概率"]
def category_learning(worker, job):
    title = job.data
    print "%s get called, data: %s" % (strftime("%Y-%m-%d %H:%M:%S", gmtime()), title)
    title_classes = title_clf.predict_proba([title])
    sorted_labels = np.argsort(title_classes[0]).tolist()
    sorted_labels.reverse()

    result = [(first_category_name[parentmap[train_names[j]]],
               namemap[train_names[j]],
               title_classes[0][j] * 100)
             for j in sorted_labels[:3]]
    return json.dumps(result)


# 注册worker
new_worker = gearman.GearmanWorker(GEARMAN_IPS)
print 'register worker'
new_worker.register_task("category_learning", category_learning)
print 'start to work...'
new_worker.work()
