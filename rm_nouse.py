# -*- coding=utf-8 -*-

"""
author: liqiping@baixing.net
class_count.json是一个统计每一个二级类目包含信息条数的json文件，
把不在这个统计之中的二级类目从数据集中移除
"""

import json
from os import listdir
from os.path import isdir, join
from shutil import rmtree

with open("./class_count.json") as f:
    class_count = json.load(f)

def rm_folds(directory_name):
    for f in listdir(directory_name):
        if isdir(join(directory_name, f)) and f not in class_count:
            rmtree(join(directory_name, f))

rm_folds('/home/liqiping/Documents/mldata-05-20')
rm_folds('/home/liqiping/Documents/mltest-05-04')
