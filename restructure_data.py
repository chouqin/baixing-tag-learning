# -*- coding=utf-8 -*-

"""
author: liqiping@baixing.net
将层次目录结构的数据集转换为一个文本文件,
文件的每一行是一个样本，格式为：
类目英文名 文本
"""

from load_files import load_title_desc

train_container_path = "/run/mldata_parent"
test_container_path = "/run/mltestdata_parent"

train_file = open("./train_parent", "w")
test_file = open("./test_parent", "w")

train_data = load_title_desc(train_container_path)
X_title = train_data['title']
y_train = train_data['target']
target_names = train_data['target_names']

for i in range(len(X_title)):
    train_file.write("%s\t%s" % (target_names[y_train[i]], X_title[i]))

test_data = load_title_desc(test_container_path)
X_title = test_data['title']
y_train = test_data['target']
target_names = test_data['target_names']

for i in range(len(X_title)):
    test_file.write("%s\t%s" % (target_names[y_train[i]], X_title[i]))

train_file.close()
test_file.close()
