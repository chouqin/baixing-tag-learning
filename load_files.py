# -*- coding=utf-8 -*-

"""
author: liqiping@baixing.net

根据目录结构生成数据集，同时从每一个数据集的样本中提取出标题和描述
"""

from sklearn.datasets import load_files

split_str = "----------------------------------baixing\n"

def load_title_desc(container_path, limit = None):
    dataset = load_files(container_path)

    title_data = []
    desc_data = []
    if limit is None:
        limit = len(dataset.data)

    for data in dataset.data[:limit]:
        try:
            title, desc = data.split(split_str)
        except:
            title, desc = data, ''
        title_data.append(title)
        desc_data.append(title + desc)

    result = {
        'title': title_data,
        'desc': desc_data,
        'target': dataset.target[:limit],
        'target_names': dataset.target_names[:limit],
        'DESCR': dataset.DESCR,
    }

    return result
