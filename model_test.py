#!/usr/bin/env python
# -*- coding=utf-8 -*-

"""
author: liqiping@baixing.net

测试分类器
"""

import os
import time
from datetime import datetime
from os.path import join

from sklearn.externals import joblib
from sklearn.datasets import load_files

import numpy as np
import json
import csv

from os.path import split

from configs import test_data_path
from model_train import predict, fitter, classfiers

def format_result(result):
    max_prob, title, actual_label, selected_labels = result
    return "%s\n%s\n%s %s\n" % (max_prob, title, actual_label, ' '.join(selected_labels))

def log_predicte_result(result, result_dir='./result/'):
    u'''
    记录测试结果
    '''
    timestr = datetime.now().strftime("%m%d%H%M")
    result_dir = join(result_dir, timestr)
    if not os.path.exists(result_dir):
        os.mkdir(result_dir)

    conclude = open(join(result_dir, 'conclude'), 'w')
    conclude.write('testsize:%s\n' % result['test_size'])
    conclude.write('correct count:%s\n' % result['correct_count'])
    conclude.write('correct rate:%s\n' % (float(result['correct_count']) / result['test_size']))
    conclude.write('exact correct rate:%s\n' % (float(result['score_count']) / result['test_size']))
    conclude.write('score:%s\n' % result['score'])
    conclude.close()

    for key in ['unsure_correct', 'unsure_wrong', 'sure_wrong', 'sure_correct']:
        output = open(join(result_dir, key), 'w')
        output.write('\n'.join(map(format_result, result[key])))
        output.close()
    return result_dir


def test_model(clf_pkl, names_pkl, test_data_path, result_dir = './result/'):
    # 加载训练好的模型
    title_clf = joblib.load(clf_pkl)
    train_names = joblib.load(names_pkl)

    result = predict(title_clf, train_names, test_data_path)
    return log_predicte_result(result)

def test_schema(datapath='/home/ls/mldata/mltitledata', testdatapath='/home/ls/mldata/mltitletestdata', clfname = 'ovr_sgd', trlimit = None, telimit = None):
    start = time.time()
    clf, names = fitter(datapath, clfname, limit = trlimit)
    result_dir = log_predicte_result(predict(clf, names, testdatapath, limit = telimit))
    print "classifier:%s\ntrain data limit:%s\ntest data limit:%s\ntime used: %ss\n" % (clfname, trlimit or 'Nan', telimit or 'Nan', time.time() - start)
    print "result conclude:\n"
    print open(join(result_dir,'conclude')).read()


if __name__ == '__main__':
    for clf in ['ovr_sgd_nlpir']:
        #test_schema(clfname = clf, testdatapath='/home/ls/mldata/mltest-05-04/', trlimit = None, telimit=20000)
        #test_schema(clfname = clf, testdatapath='/home/ls/mldata/mltest-05-05', trlimit = None, telimit=20000)
        test_schema(clfname = clf, datapath='/home/ls/mldata/mldata-08-23', testdatapath='/home/ls/mldata/mltestdata-08-23')
