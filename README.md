##这是什么

这个项目用于百姓网的类目自动推荐，是一个正在进行中的项目。

##使用到的库

* [scikit-learn](http://scikit-learn.org): 机器学习的Python库，安装方法见官方文档
* jieba: 一个分词的Python库
* Flask: 用于展示效果
* pyquery: 用于解析html文件，提取类目的信息
