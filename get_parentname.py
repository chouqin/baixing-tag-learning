# -*- coding=utf-8 -*-

"""
author: liqiping@baixing.net
根据http://www.baixing.com/pages/test/category_meta.php上的内容提取二级类目
与一级类目的对应关系，保存成一个json文件
"""

from pyquery import PyQuery as pq
import json

d = pq(filename='./parentname.html')
trs = d("tr")

parentlist = []
parentmap = {}

for i in range(1, len(trs)):
    tr = trs.eq(i)
    tds = tr.find("td")
    if tds is None or len(tds) == 0:
        continue

    subname = tds.eq(1).text()
    parentname = tds.eq(3).text()

    if parentname == "":
        continue

    if parentname not in parentlist:
        parentlist.append(parentname)

    parentmap[subname] = parentname

with open("./parent_map.json", "w") as f:
    json.dump(parentmap, f)
